/**
 * CompanyPosition.java
*/

package org.com1027.formative.ac01271;

/**
 * @author ac01271
 */

public class CompanyPosition {

	/** Default Employee Position Name **/
	private final static String DEFAULT_POSITION_NAME = "Unemployed";
	
	/** Employee's Position Name */
	private String positionName = DEFAULT_POSITION_NAME;

	/** Default Constructor that constructs a new CompanyPosition object */
	public CompanyPosition() {
		super();
	}

	/**
	 * @return Employee's Position Name
	 */
	public String getPositionName() {
		return this.positionName;
	}

	/**
	 * Procedure that amends an Employee's Position Name
	 * 
	 * @param Employee's New Position Name
	 */
	public void setPositionName(String positionName) {
		// Try method to catch any occurring exceptions
		try {
			this.positionName = positionName;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
