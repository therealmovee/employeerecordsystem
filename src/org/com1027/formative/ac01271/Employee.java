/**
 * Employee.java
 */

package org.com1027.formative.ac01271;

/**
 * @author ac01271
 */

public class Employee {

	/** Employee's ID */
	private int id = 0;

	/** Employee's first name */
	private String forename = "";

	/** Employee's surname */
	private String surname = "";

	/** Employee's salary */
	private Salary salary = null;

	/** Employee's company position name */
	private CompanyPosition companyPosition = null;

	/** Minimum salary to be eligible for a bonus */
	private static final double REQUIRED_SALARY_FOR_BONUS = 40000;

	/**
	 * Parameterized Constructor that constructs a new Employee object
	 * 
	 * @param id              Employee's ID
	 * @param forename        Employee's First Name
	 * @param surname         Employee's Surname
	 * @param salary          Employee's Surname
	 * @param companyPosition Employee's Position Name
	 */
	public Employee(int id, String forename, String surname, Salary salary, CompanyPosition companyPosition) {
		super();

		// Validating all parameters to prevent any unexpected errors
		validateEmployeeDetails(id, forename, surname, salary, companyPosition);

		try {
			// Initializing the fields
			this.id = id;
			this.salary = salary;
			this.companyPosition = companyPosition;

			// Ensuring that the first letter is a capital.
			this.forename = forename;
			this.surname = surname;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return Employee's ID
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return Employee's First Name
	 */
	public String getForename() {
		return this.forename;
	}

	/**
	 * @return Employee's Surname
	 */
	public String getSurname() {
		return this.surname;
	}

	/**
	 * @return Employee's Salary
	 */
	public double getSalary() {
		return this.salary.getSalary();
	}

	/**
	 * @return Employee's Position Name
	 */
	public String getPositionName() {
		return this.companyPosition.getPositionName();
	}

	/**
	 * @return True/False depending if the Employee is eligible for bonus
	 */
	public boolean eligibleForBonus() {
		// Try method to catch any occurring exceptions
		try {
			return salary.getSalary() >= REQUIRED_SALARY_FOR_BONUS ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Procedure that corrects/adjusts any invalid parameters to prevent unexpected
	 * errors
	 * 
	 * @param id              Employee's ID
	 * @param forename        Employee's First Name
	 * @param surname         Employee's Surname
	 * @param salary          Employee's Surname
	 * @param companyPosition Employee's Position Name
	 */
	public void validateEmployeeDetails(int id, String forename, String surname, Salary salary,
			CompanyPosition companyPosition) {
		// Try method to catch any occurring exceptions
		try {
			/** Checking if ID is within Integer boundaries */
			if (id < Integer.MIN_VALUE || id > Integer.MAX_VALUE)
				id = 0;

			/** Checking if fore name is null */
			if (forename == null || forename.isEmpty())
				forename = " ";

			/** Checking if surname is null */
			if (surname == null || surname.isEmpty())
				surname = " ";

			/** Checking if Salary Object is null */
			if (salary == null)
				salary = new Salary();

			/** Checking if CompanyPosition Object is null */
			if (companyPosition == null)
				companyPosition = new CompanyPosition();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Overrides the default toString() and formats the String Representation of the
	 * Employee Object appropriately
	 */
	@Override
	public String toString() {
		// Try method to catch any occurring exceptions
		try {
			// Validating all fields to prevent any unexpected errors
			validateEmployeeDetails(this.id, this.forename, this.surname, this.salary, this.companyPosition);

			// Concatenating all the fields into a single string
			return this.surname + ", " + this.forename + " (" + this.id + "): " + this.getPositionName() + " at �"
					+ this.getSalary() + " (�" + salary.calculateTax() + " tax) and is "
					+ (eligibleForBonus() ? "" : "not ") + "eligible for bonus.";
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

}
