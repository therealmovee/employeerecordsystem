/**
 * Employee.java
 */

package org.com1027.formative.ac01271.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ac01271
 */

public class Employee {

	/** Employee's ID */
	private int id = 0;

	/** Employee's first name */
	private String forename = "";

	/** Employee's surname */
	private String surname = "";

	/** Employee's salary */
	private Salary salary = null;

	/** Employee's company position name */
	private CompanyPosition companyPosition = null;

	/** Minimum salary to be eligible for a bonus */
	private static final double REQUIRED_SALARY_FOR_BONUS = 40000;

	/**
	 * Regular Expression to check if a string starts with a lower case character
	 */
	private static final String CHARACTERS_REGEX = "^[a-z][a-zA-Z].*";

	/**
	 * Parameterized Constructor that constructs a new Employee object
	 * 
	 * @param id              Employee's ID
	 * @param forename        Employee's First Name
	 * @param surname         Employee's Surname
	 * @param salary          Employee's Surname
	 * @param companyPosition Employee's Position Name
	 */
	public Employee(int id, String forename, String surname, Salary salary, CompanyPosition companyPosition) {
		super();

		// Validating all parameters to prevent any unexpected errors
		validateEmployeeDetails(id, forename, surname, salary, companyPosition);

		try {
			// Initializing the fields
			this.id = id;
			this.salary = salary;
			this.companyPosition = companyPosition;

			// Ensuring that the first letter is a capital.
			this.forename = capitalize(forename);
			this.surname = capitalize(surname);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return Employee's ID
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return Employee's First Name
	 */
	public String getForename() {
		return this.forename;
	}

	/**
	 * @return Employee's Surname
	 */
	public String getSurname() {
		return this.surname;
	}

	/**
	 * @return Employee's Salary
	 */
	public double getSalary() {
		return this.salary.getSalary();
	}

	/**
	 * @return Employee's Position Name
	 */
	public String getPositionName() {
		return this.companyPosition.getPositionName();
	}

	/**
	 * @return True/False depending if the Employee is eligible for bonus
	 */
	public boolean eligibleForBonus() {
		// Try method to catch any occurring exceptions
		try {
			return salary.getSalary() >= REQUIRED_SALARY_FOR_BONUS ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Procedure that corrects/adjusts any invalid parameters to prevent unexpected
	 * errors
	 * 
	 * @param id              Employee's ID
	 * @param forename        Employee's First Name
	 * @param surname         Employee's Surname
	 * @param salary          Employee's Surname
	 * @param companyPosition Employee's Position Name
	 */
	public void validateEmployeeDetails(int id, String forename, String surname, Salary salary,
			CompanyPosition companyPosition) {
		// Try method to catch any occurring exceptions
		try {
			/** Checking if ID is negative or beyond max integer value */
			if (id < 0 || id > Integer.MAX_VALUE)
				id = 0;

			/** Checking if fore name is null */
			if (forename == null || forename.isEmpty())
				forename = " ";

			/** Checking if surname is null */
			if (surname == null || surname.isEmpty())
				surname = " ";

			/** Checking if Salary Object is null */
			if (salary == null)
				salary = new Salary();

			/** Checking if CompanyPosition Object is null */
			if (companyPosition == null)
				companyPosition = new CompanyPosition();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Function that capitalizes a given string using Regular Expressions
	 * 
	 * @param Non-Capitalized Name
	 * @return Capitalized Name
	 */
	public static String capitalize(String name) {
		// Try method to catch any occurring exceptions
		try {
			// Compiling the pattern & using a matcher
			// to check if word matches the pattern
			Pattern pattern = Pattern.compile(CHARACTERS_REGEX);
			Matcher matcher = pattern.matcher(name);

			StringBuffer stringBuffer = new StringBuffer();

			// Name matches the pattern (starts with lower case)
			if (matcher.find()) {
				// Replace first letter with a capital letter.
				matcher.appendReplacement(stringBuffer, matcher.group().toUpperCase().substring(0, 1)
						+ matcher.group().substring(1, matcher.group().length()));
			} else { // Name doesn't match the pattern (starts with upper case)
				return name;
			}

			return stringBuffer.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return name;
		}
	}

	/**
	 * Overrides the default toString() and formats the String Representation of the
	 * Employee Object appropriately using StringBuffer
	 */
	@Override
	public String toString() {
		// Try method to catch any occurring exceptions
		try {
			// Validating all fields to prevent any unexpected errors
			validateEmployeeDetails(this.id, this.forename, this.surname, this.salary, this.companyPosition);

			// Concatenating all the fields into a single string
			StringBuffer outputString = new StringBuffer();
			outputString.append(this.surname).append(", ");
			outputString.append(this.forename).append(" (");
			outputString.append(this.id).append("): ");
			outputString.append(this.getPositionName()).append(" at �");
			outputString.append(this.getSalary()).append(" (�");
			outputString.append(this.salary.calculateTax()).append(" tax) and is ");

			// Checking if the Employee is eligible for bonus
			if (!eligibleForBonus())
				outputString.append("not ");

			outputString.append("eligible for bonus.");

			return outputString.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

}
