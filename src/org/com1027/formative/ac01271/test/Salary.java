/**
 * Salary.java
 */

package org.com1027.formative.ac01271.test;

/**
 * @author ac01271
 */

public class Salary {

	/** Employee's Salary */
	private double salary = 0;

	/** Salary that is not taxable */
	private static final double PERSONAL_ALLOWANCE_LIMIT = 9440;
	
	/** If salary exceeds this threshold, 40% Tax is applied */
	private final static double MAXIMUM_THRESHOLD = 32010;

	/** Default Constructor that constructs a new Salary object */
	public Salary() {
		super();
	}

	/**
	 * @return Employee's Salary
	 */
	public double getSalary() {
		return salary;
	}

	/**
	 * Procedure that amends an Employee's Salary
	 * 
	 * @param The new Employee's Salary
	 */
	public void setSalary(double salary) {
		// Try method to catch any occurring exceptions
		try {
			// Preventing salary from being set to a negative value.
			this.salary = salary >= 0 ? salary : this.salary;
		} catch(Exception e) {
			System.out.println("Error occured (setSalary method): " + e.getMessage());
		}
	}

	/**
	 * Calculates and returns the total tax based on the salary of an employee.
	 * 
	 * Tax Rates: Salary(<= 9440) 0% Tax
	 * 
	 * Salary(> 9440) 20% Tax on the Salary Above 9440
	 * 
	 * Salary(> 32010) 20% Tax on the Salary Above 9440 + 40% Tax on the Remainder
	 * Salary
	 */
	public double calculateTax() {
		// Try method to catch any occurring exceptions
		try {
			/** Total Tax */
			double calculatedTax = 0;
	
			/** Salary that can be taxed */
			double taxableSalary = this.salary;
	
			// Calculating total tax
	
			if (this.salary <= PERSONAL_ALLOWANCE_LIMIT) { // Salary below/equal to 9440
				calculatedTax = 0;
			} else if (this.salary <= MAXIMUM_THRESHOLD) {
				// Deducting the Personal allowance limit
				taxableSalary -= PERSONAL_ALLOWANCE_LIMIT;
	
				// Applying 20% tax on the remainder Salary above 9440
				calculatedTax += (taxableSalary * 20) / 100;
			} else { // If the salary is above 32010
				// Deducting the personal allowance limit
				taxableSalary -= PERSONAL_ALLOWANCE_LIMIT;
	
				// Applying 20% tax on the remainder salary above 9440
				calculatedTax += (MAXIMUM_THRESHOLD * 20) / 100;
	
				taxableSalary -= MAXIMUM_THRESHOLD; // deducting 32010 to find the remainder salary amount
				calculatedTax += (taxableSalary * 40) / 100; // applying 40% tax on the remainder salary above 32010
			}
	
			return calculatedTax;
		} catch(Exception e) {
			System.out.println("Error occured (calculateTax method): " + e.getMessage());
			return 0;
		}
	}

}
